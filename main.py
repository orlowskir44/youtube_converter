from pytube import YouTube, Playlist
import os
from tkinter import *
from tkinter import messagebox
from pytube.exceptions import RegexMatchError


# https://www.youtube.com/watch?v=iuNa2AZ9roI&ab_channel=DuckSauce
# songs = []

def List():
    playlist = playlist_e.get()
    print(playlist)
    try:
        p = Playlist(playlist)
    except RegexMatchError:
        messagebox.showinfo('Bad Link', 'Bad link, RegexMatchError')
    except KeyError:
        messagebox.showinfo('Bad Link', 'Bad link')
    # TODO: Bad link to list -> error
    else:
        for i in p.videos:
            Download(i)


def Song():
    song = song_e.get()
    try:
        link = YouTube(song)
    except RegexMatchError:
        messagebox.showinfo('Bad Link', 'Bad link, RegexMatchError')
    else:
        Download(link)


def Download(convert):
    File = convert.streams.filter(only_audio=True).first()

    Mp4 = File.download()

    base, ext = os.path.splitext(Mp4)
    Mp3 = base + '.mp3'
    os.rename(Mp4, Mp3)
    #TODO: download -> info
    # songs.append(convert.title)
    # messagebox.showinfo(title='Gotowe!',message=f"Pobrano! Nazwa utworu:{songs}")


# ---------------------------- UI SETUP ------------------------------- #

# Window:
window = Tk()
window.title("YouTube converter")
window.config(padx=45, pady=25)
#

# Image:
canvas = Canvas(width=128, height=128, highlightthickness=0)
photo = PhotoImage(file="yt.png")
canvas.create_image(64, 64, image=photo)
canvas.grid(column=1, row=0, pady=25)
#

# Song download:
Label(text="Download Song:").grid(column=0, row=1)
song_e = Entry()
song_e.grid(column=1, row=1, columnspan=2)

song_b = Button(text='Download Song', command=Song)
song_b.grid(column=4, row=1)

# Playlist download:
Label(text="Download Playlist:").grid(column=0, row=3)
playlist_e = Entry()
playlist_e.grid(column=1, row=3, columnspan=2)

playlist_b = Button(text='Download Playlist', command=List)
playlist_b.grid(column=4, row=3)

window.mainloop()
